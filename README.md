# Crypto Advent Calendar Whitepaper

## Contracts

| Name | Symbol | Purpose |
| -- | -- | -- |
| HolidayToken | HOLIDAY | Token as a reward for opening doors and buying gifts. |
| GiftToken | GIFT | NFT buyable with HOLIDAY tokens and openable to get rewards. Picture is generated on-chain and each GIFT is unique. |
| Door | - | Smart contract for the game itself. Contains logic to open a door every day. Opening will be rewarded by HOLIDAY tokens and rarely a GIFT. |
| Rewards | ... | Multiple NFT with the rewards after opening a GIFT. Common and rare items. |
| NorthPole | - | Contract where GIFTs can be staked to increase the level and earn HOLIDAY TOKENS. |

## Gift

1 - 2
2 - 4
3 - 8
4 - 16
5 - 32

| Property | Values | Bits |
| -- | -- | -- |
| Gift form | {square, high, flat, round} | 2 |
| Multiple base color | {yes, no} | 1 |
| Base color | {red, blue, yellow, orange, purple, green} | 3 |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |
|  |  |  |

### Resources

https://developer.mozilla.org/en-US/docs/Web/SVG/Element/feColorMatrix

## Random events

To get random numbers a third party must be paid.
To make the system scalable this fee must be covered by the user.
Therefor the user must send wei with the transaction when interacting with an function that needs randomness.

- Open door
- Unstake
- Open/Burn gift

